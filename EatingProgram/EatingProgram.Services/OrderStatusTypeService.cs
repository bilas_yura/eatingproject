﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.DTO;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.Entities;
using System;

namespace EatingProgram.Services
{
    public class OrderStatusTypeService : IOrderStatusTypeService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public OrderStatusTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<OrderStatusTypeDto> Get()
        {
            var orderStatusTypes = _unitOfWork.OrderStatusTypes.GetAll();
            return _mapper.Map(orderStatusTypes, new List<OrderStatusTypeDto>());
        }

        public OrderStatusTypeDto GetById(int id)
        {
            OrderStatusType orderStatusType = _unitOfWork.OrderStatusTypes.GetById(id);
            if (orderStatusType == null)
                throw new Exception("No order status was found");
            return _mapper.Map(orderStatusType, new OrderStatusTypeDto());
        }
    }
}
