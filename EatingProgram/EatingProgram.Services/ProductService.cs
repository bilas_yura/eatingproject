﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.DTO;
using System.Collections.Generic;
using System.Linq;
using EatingProgram.Core.Entities;
using EatingProgram.Core.Abstractions.Services;
using System;

namespace EatingProgram.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<ProductDto> GetByProductType(int typeId)
        {
            var products = _unitOfWork.Products
                .Find(prod => prod.ProductTypeId == typeId);
            return _mapper.Map(products, new List<ProductDto>());
        }

        public List<ProductDto> GetByOrderId(int id)
        {
            var products = _unitOfWork.Products
                .Find(prod => prod.OrderItems.Any(oitem => oitem.OrderId == id));
            return _mapper.Map(products, new List<ProductDto>());
        }

        public List<ProductDto> Get()
        {
            var products = _unitOfWork.Products.GetAll();
            return _mapper.Map(products, new List<ProductDto>());
        }

        public ProductWithTypeDto GetProductWithTypeById(int id)
        {
            Product product = _unitOfWork.Products.GetProductWithTypeById(id);
            if (product == null)
                throw new Exception("No product was found");
            return _mapper.Map(product, new ProductWithTypeDto());
        }

        public ProductDto GetById(int id)
        {
            Product product = _unitOfWork.Products.GetById(id);
            if (product == null)
                throw new Exception("No product was found");
            return _mapper.Map(product, new ProductDto());
        }

        public ProductDto Insert(ProductDto product)
        {
            Product valueToInsert = _mapper.Map(product, new Product());
            _unitOfWork.Products.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new ProductDto());
        }

        public ProductDto Update(ProductDto product)
        {
            var existing = _unitOfWork.Products.GetById(product.Id);
            existing.Name = product.Name;
            existing.Price = product.Price;
            existing.ProductTypeId = product.ProductTypeId;
            _unitOfWork.Products.Update(existing);
            _unitOfWork.SaveChanges();
            return _mapper.Map(_unitOfWork.Products.GetById(product.Id), new ProductDto());
        }

        public void Delete(int id)
        {
            _unitOfWork.Products.DeleteById(id);
            _unitOfWork.SaveChanges();
        }
    }
}
