﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using EatingProgram.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EatingProgram.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public OrderItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<OrderItemDto> GetByProductType(int typeId)
        {
            var orderItems = _unitOfWork.OrderItems
                .Find(oitem => oitem.Product.ProductTypeId == typeId);
            return _mapper.Map(orderItems, new List<OrderItemDto>());
        }

        public List<OrderItemDto> GetByOrderId(int id)
        {
            var orderItems = _unitOfWork.OrderItems
                .Find(oitem => oitem.OrderId == id);
            return _mapper.Map(orderItems, new List<OrderItemDto>());
        }

        public List<OrderItemDto> Get()
        {
            var orderItems = _unitOfWork.OrderItems.GetAll();
            return _mapper.Map(orderItems, new List<OrderItemDto>());
        }

        public OrderItemDto GetById(int id)
        {
            OrderItem orderItem = _unitOfWork.OrderItems.GetById(id);
            if (orderItem == null)
                throw new Exception("No order item was found");
            return _mapper.Map(orderItem, new OrderItemDto());
        }

        public OrderItemDto Insert(OrderItemDto orderItem)
        {
            OrderItem valueToInsert = _mapper.Map(orderItem, new OrderItem());
            _unitOfWork.OrderItems.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new OrderItemDto());
        }

        public OrderItemDto Update(OrderItemDto orderItem)
        {
            var existing = _unitOfWork.OrderItems.GetById(orderItem.Id);
            existing.Description = orderItem.Description;
            existing.Quantity = orderItem.Quantity;
            existing.ProductId = orderItem.ProductId;
            _unitOfWork.OrderItems.Update(existing);
            _unitOfWork.SaveChanges();
            return _mapper.Map(_unitOfWork.OrderItems.GetById(orderItem.Id), new OrderItemDto());
        }

        public void Delete(int id)
        {
            _unitOfWork.OrderItems.DeleteById(id);
            _unitOfWork.SaveChanges();
        }
    }
}
