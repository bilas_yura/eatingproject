﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using EatingProgram.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EatingProgram.Services
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public ProductTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<ProductTypeDto> Get()
        {
            var productTypes = _unitOfWork.ProductTypes.GetAll();
            return _mapper.Map(productTypes, new List<ProductTypeDto>());
        }

        public ProductTypeDto GetById(int id)
        {
            ProductType productType = _unitOfWork.ProductTypes.GetById(id);
            if (productType == null)
                throw new Exception("No product was found");
            return _mapper.Map(productType, new ProductTypeDto());
        }

        public ProductTypeDto Insert(ProductTypeDto productType)
        {
            ProductType valueToInsert = _mapper.Map(productType, new ProductType());
            _unitOfWork.ProductTypes.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new ProductTypeDto());
        }

        public ProductTypeDto Update(ProductTypeDto productType)
        {
            var existing = _unitOfWork.ProductTypes.GetById(productType.Id);
            existing.Name = productType.Name;
            _unitOfWork.ProductTypes.Update(existing);
            _unitOfWork.SaveChanges();
            return _mapper.Map(_unitOfWork.ProductTypes.GetById(productType.Id), new ProductTypeDto());
        }

        public void Delete(int id)
        {
            _unitOfWork.ProductTypes.DeleteById(id);
            _unitOfWork.SaveChanges();
        }
    }
}
