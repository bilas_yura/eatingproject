﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using EatingProgram.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EatingProgram.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<OrderDto> GetByProductId(int id)
        {
            var orders =  _unitOfWork.Orders
                .Find(ord => ord.OrderItems.Any(oitem => oitem.ProductId == id)).ToList();
            return _mapper.Map(orders, new List<OrderDto>());
        }

        public OrderWithItemsDto GetOrderWithItemsById(int id)
        {
            Order order = _unitOfWork.Orders.GetByIdIncludingItems(id);
            if (order == null)
                throw new Exception("No order was found");
            return _mapper.Map(order, new OrderWithItemsDto());
        }

        public List<OrderDto> GetByProductType(int typeId)
        {
            var orders = _unitOfWork.Orders
                .Find(ord => ord.OrderItems.Any(oitem => oitem.Product.ProductTypeId == typeId));
            return _mapper.Map(orders, new List<OrderDto>());
        }

        public List<OrderDto> GetByStatusId(int id)
        {
            var orders = _unitOfWork.Orders
                .Find(ord => ord.StatusId == id);
            return _mapper.Map(orders, new List<OrderDto>());
        }

        public List<OrderDto> GetByUserId(int id)
        {
            var orders = _unitOfWork.Orders
                .Find(ord => ord.UserId == id);
            return _mapper.Map(orders, new List<OrderDto>());
        }

        public List<OrderDto> Get()
        {
            var orders = _unitOfWork.Orders.GetAll();
            return _mapper.Map(orders, new List<OrderDto>());
        }

        public OrderDto GetById(int id)
        {
            Order order = _unitOfWork.Orders.GetById(id);
            if (order == null)
                throw new Exception("No order was found");
            return _mapper.Map(order, new OrderDto());
        }

        public OrderDto Insert(OrderDto order)
        {
            Order valueToInsert = _mapper.Map(order, new Order());
            _unitOfWork.Orders.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new OrderDto());
        }

        public OrderWithItemsDto Insert(OrderWithItemsDto order)
        {
            Order valueToInsert = _mapper.Map(order, new Order());
            _unitOfWork.Orders.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new OrderWithItemsDto());
        }

        public OrderDto Update(OrderDto order)
        {
            var existing = _unitOfWork.Orders.GetById(order.Id);
            existing.UpdatedTime = order.UpdatedTime;
            existing.AdditionalInfo = order.AdditionalInfo;
            existing.StatusId = order.StatusId;
            existing.UserId = order.UserId;
            if (order.OrderTime != null)
                existing.OrderTime = order.OrderTime.Value;
            _unitOfWork.Orders.Update(existing);
            _unitOfWork.SaveChanges();
            return _mapper.Map(_unitOfWork.Orders.GetById(order.Id), new OrderDto());
        }

        public void Delete(int id)
        {
            _unitOfWork.Orders.DeleteById(id);
            _unitOfWork.SaveChanges();
        }
    }
}
