﻿using AutoMapper;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using EatingProgram.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EatingProgram.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<UserDto> Get()
        {
            var users = _unitOfWork.Users.GetAll();
            return _mapper.Map(users, new List<UserDto>());
        }

        public UserDto GetById(int id)
        {
            User user = _unitOfWork.Users.GetById(id);
            if(user == null)
                throw new Exception("No user was found");
            return _mapper.Map(user, new UserDto());
        }

        public UserDto Insert(UserDto user)
        {
            User valueToInsert = _mapper.Map(user, new User());
            _unitOfWork.Users.Add(valueToInsert);
            _unitOfWork.SaveChanges();
            return _mapper.Map(valueToInsert, new UserDto());
        }

        public UserDto Update(UserDto user)
        {
            var existing = _unitOfWork.Users.GetById(user.Id);
            existing.FirstName = user.FirstName;
            existing.LastName = user.LastName;
            existing.BirthDate = user.BirthDate;
            _unitOfWork.Users.Update(existing);
            _unitOfWork.SaveChanges();
            return _mapper.Map(_unitOfWork.Users.GetById(user.Id), new UserDto());
        }

        public void Delete(int id)
        {
            _unitOfWork.Users.DeleteById(id);
            _unitOfWork.SaveChanges();
        }
    }
}
