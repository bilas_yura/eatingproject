﻿using AutoMapper;
using EatingProgram.Core.DTO;
using EatingProgram.Core.Entities;

namespace EatingProgram.Core
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<OrderDto, Order>();
            CreateMap<Order, OrderDto>();

            CreateMap<OrderWithItemsDto, Order>();
            CreateMap<Order, OrderWithItemsDto>();

            CreateMap<OrderItemDto, OrderItem>();
            CreateMap<OrderItem, OrderItemDto>();

            CreateMap<OrderStatusTypeDto, OrderStatusType>();
            CreateMap<OrderStatusType, OrderStatusTypeDto>();

            CreateMap<ProductDto, Product>();
            CreateMap<Product, ProductDto>();

            CreateMap<ProductWithTypeDto, Product>();
            CreateMap<Product, ProductWithTypeDto>();

            CreateMap<ProductTypeDto, ProductType>();
            CreateMap<ProductType, ProductTypeDto>();

            CreateMap<UserDto, User>();
            CreateMap<User, UserDto>()
                .ForMember(userDto => userDto.FullName, 
                           user => user.MapFrom(x => x.FirstName + " " + x.LastName));
        }
    }
}
