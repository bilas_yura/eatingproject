﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EatingProgram.Core.Entities
{
    public class Order : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime? OrderTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        [StringLength(256)]
        public string AdditionalInfo { get; set; }
        public int StatusId { get; set; }
        public OrderStatusType Status { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public List<OrderItem> OrderItems { get; set; }
    }
}
