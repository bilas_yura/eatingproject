﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EatingProgram.Core.Entities
{
    public class ProductType : IEntity<int>
    {
        public int Id { get; set; }
        [StringLength(64)]
        public string Name { get; set; }
        public List<Product> Products { get; set; }
    }
}
