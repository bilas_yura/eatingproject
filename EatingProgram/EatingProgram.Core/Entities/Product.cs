﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EatingProgram.Core.Entities
{
    public class Product : IEntity<int>
    {
        public int Id { get; set; }
        [StringLength(64)]
        public string Name { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        public int ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }
        public List<OrderItem> OrderItems { get; set; }
    }
}
