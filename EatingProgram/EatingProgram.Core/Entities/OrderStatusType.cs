﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EatingProgram.Core.Entities
{
    public class OrderStatusType : IEntity<int>
    {
        public int Id { get; set; }
        [StringLength(16)]
        public string Name { get; set; }
        public List<Order> Orders { get; set; }
    }
}
