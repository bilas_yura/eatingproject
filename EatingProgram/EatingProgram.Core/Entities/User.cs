﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EatingProgram.Core.Entities
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        [StringLength(32)]
        public string FirstName { get; set; }
        [StringLength(32)]
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public List<Order> Orders { get; set; }
    }
}
