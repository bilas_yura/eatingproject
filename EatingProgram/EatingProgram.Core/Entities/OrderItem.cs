﻿using System.ComponentModel.DataAnnotations;

namespace EatingProgram.Core.Entities
{
    public class OrderItem : IEntity<int>
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
