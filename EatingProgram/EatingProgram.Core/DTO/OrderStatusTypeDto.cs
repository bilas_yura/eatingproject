﻿namespace EatingProgram.Core.DTO
{
    public class OrderStatusTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
