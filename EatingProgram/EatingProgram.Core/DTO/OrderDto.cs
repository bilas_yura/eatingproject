﻿using System;

namespace EatingProgram.Core.DTO
{
    public class OrderDto
    {
        public int Id { get; set; }
        public DateTime? OrderTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public string AdditionalInfo { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
    }
}
