﻿using System;
using System.Collections.Generic;

namespace EatingProgram.Core.DTO
{
    public class OrderWithItemsDto
    {
        public int Id { get; set; }
        public DateTime? OrderTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public string AdditionalInfo { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public List<OrderItemDto> OrderItems { get; set; }
    }
}
