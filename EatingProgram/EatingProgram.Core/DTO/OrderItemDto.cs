﻿namespace EatingProgram.Core.DTO
{
    public class OrderItemDto
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
    }
}
