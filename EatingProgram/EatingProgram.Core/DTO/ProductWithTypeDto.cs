﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatingProgram.Core.DTO
{
    public class ProductWithTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public ProductTypeDto ProductType { get; set; }
    }
}
