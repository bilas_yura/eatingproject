﻿using EatingProgram.Core.DTO;
using System.Collections.Generic;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IProductService : IService<ProductDto>
    {
        ProductWithTypeDto GetProductWithTypeById(int id);
        List<ProductDto> GetByProductType(int typeId);
        List<ProductDto> GetByOrderId(int id);
    }
}
