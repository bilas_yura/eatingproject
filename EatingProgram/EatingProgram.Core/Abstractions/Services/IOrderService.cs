﻿using EatingProgram.Core.DTO;
using System.Collections.Generic;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IOrderService : IService<OrderDto> 
    {
        List<OrderDto> GetByProductId(int id);
        OrderWithItemsDto GetOrderWithItemsById(int id);
        List<OrderDto> GetByProductType(int typeId);
        List<OrderDto> GetByUserId(int id);
        List<OrderDto> GetByStatusId(int i);
        OrderWithItemsDto Insert(OrderWithItemsDto order);
    }
}
