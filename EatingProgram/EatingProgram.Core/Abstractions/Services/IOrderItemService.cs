﻿using EatingProgram.Core.DTO;
using System.Collections.Generic;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IOrderItemService : IService<OrderItemDto> 
    {
        List<OrderItemDto> GetByProductType(int typeId);
        List<OrderItemDto> GetByOrderId(int id);
    }
}
