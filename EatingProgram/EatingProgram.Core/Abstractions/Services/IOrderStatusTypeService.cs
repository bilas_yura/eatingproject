﻿using EatingProgram.Core.DTO;
using System.Collections.Generic;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IOrderStatusTypeService 
    {
        List<OrderStatusTypeDto> Get();
        OrderStatusTypeDto GetById(int id);
    }
}
