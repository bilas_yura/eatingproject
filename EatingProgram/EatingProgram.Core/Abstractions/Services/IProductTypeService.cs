﻿using EatingProgram.Core.DTO;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IProductTypeService : IService<ProductTypeDto> { }
}
