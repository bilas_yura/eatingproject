﻿using System.Collections.Generic;

namespace EatingProgram.Core.Abstractions.Services
{
    public interface IService<TDto> where TDto : class
    {
        public List<TDto> Get();
        public TDto GetById(int id);
        public TDto Insert(TDto user);
        public TDto Update(TDto user);
        public void Delete(int id);
    }
}
