﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;
using System;
using System.Threading.Tasks;

namespace EatingProgram.Core.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        IOrderRepository Orders { get; }
        IOrderItemRepository OrderItems { get; }
        IProductRepository Products { get; }
        IProductTypeRepository ProductTypes { get; }
        IUserRepository Users { get; }
        IOrderStatusTypeRepository OrderStatusTypes { get; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
