﻿using EatingProgram.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IRepository<TEntity, in TId> where TEntity : class, IEntity<TId>
    {
        TEntity GetById(TId id);
        Task<TEntity> GetByIdAsync(TId id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        
        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        void AddMany(IEnumerable<TEntity> entities);

        void Delete(TEntity entity);
        void DeleteById(TId id);
        void DeleteMany(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        Task<int> CountAsync();
        Task<int> CountWhereAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
