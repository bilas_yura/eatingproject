﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IUserRepository : IRepository<User, int> { }
}
