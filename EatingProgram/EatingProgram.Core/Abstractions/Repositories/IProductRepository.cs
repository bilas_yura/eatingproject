﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IProductRepository : IRepository<Product, int> 
    {
        Product GetProductWithTypeById(int id);
    }
}
