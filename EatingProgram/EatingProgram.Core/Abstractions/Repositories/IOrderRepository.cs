﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IOrderRepository : IRepository<Order, int>
    {
        Order GetByIdIncludingItems(int id);
    }
}
