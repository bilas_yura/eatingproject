﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IOrderStatusTypeRepository : IRepository<OrderStatusType, int> { }
}
