﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IProductTypeRepository : IRepository<ProductType, int> { }
}
