﻿using EatingProgram.Core.Entities;

namespace EatingProgram.Core.Abstractions.Repositories
{
    public interface IOrderItemRepository : IRepository<OrderItem, int> { }
}
