﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;

namespace EatingProgram.DAL.Repositories
{
    public class ProductTypeRepository : BaseRepository<ProductType, int>, IProductTypeRepository
    {
        public ProductTypeRepository(EatingProgramContext context) : base(context) { }
    }
}
