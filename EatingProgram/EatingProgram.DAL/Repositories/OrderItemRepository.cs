﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;

namespace EatingProgram.DAL.Repositories
{
    public class OrderItemRepository : BaseRepository<OrderItem, int>, IOrderItemRepository
    {
        public OrderItemRepository(EatingProgramContext context) : base(context) { }
    }
}
