﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;

namespace EatingProgram.DAL.Repositories
{
    public class OrderStatusTypeRepository : 
        BaseRepository<OrderStatusType, int>, IOrderStatusTypeRepository
    {
        public OrderStatusTypeRepository(EatingProgramContext context) : 
            base(context) { }
    }
}
