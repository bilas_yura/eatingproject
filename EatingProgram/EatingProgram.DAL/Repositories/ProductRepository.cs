﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;

namespace EatingProgram.DAL.Repositories
{
    public class ProductRepository : BaseRepository<Product, int>, IProductRepository
    {
        public ProductRepository(EatingProgramContext context) : base(context) { }

        public Product GetProductWithTypeById(int id)
        {
            var product = _context.Set<Product>().Find(id);
            _context.Entry(product).Reference(prod => prod.ProductType).Load();
            return product;
        }
    }
}
