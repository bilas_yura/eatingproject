﻿using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.Core.Entities;

namespace EatingProgram.DAL.Repositories
{
    public class OrderRepository : BaseRepository<Order, int>, IOrderRepository
    {
        public OrderRepository(EatingProgramContext context) : base(context) { }

        public Order GetByIdIncludingItems(int id)
        {
            var order = _context.Set<Order>().Find(id);
            _context.Entry(order).Collection(ord => ord.OrderItems).Load();
            return order;
        }
    }
}
