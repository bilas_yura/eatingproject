﻿using EatingProgram.Core.Entities;
using EatingProgram.Core.Abstractions.Repositories;

namespace EatingProgram.DAL.Repositories
{
    public class UserRepository : BaseRepository<User, int>, IUserRepository
    {
        public UserRepository(EatingProgramContext context) : base(context) { }
    }
}
