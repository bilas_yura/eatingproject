﻿using System.Threading.Tasks;
using EatingProgram.Core.Abstractions;
using EatingProgram.Core.Abstractions.Repositories;
using EatingProgram.DAL.Repositories;

namespace EatingProgram.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private EatingProgramContext _context;

        public UnitOfWork(EatingProgramContext context)
        {
            _context = context;
        }

        private IOrderRepository _orders;
        public IOrderRepository Orders => 
            _orders ??= new OrderRepository(_context);

        private IOrderItemRepository _orderItems;
        public IOrderItemRepository OrderItems => 
            _orderItems ??= new OrderItemRepository(_context);

        private IProductRepository _products;
        public IProductRepository Products => 
            _products ??= new ProductRepository(_context);

        private IProductTypeRepository _productTypes;
        public IProductTypeRepository ProductTypes => 
            _productTypes ??= new ProductTypeRepository(_context);

        private IUserRepository _users;
        public IUserRepository Users => 
            _users ??= new UserRepository(_context);

        private IOrderStatusTypeRepository _orderStatusTypes;
        public IOrderStatusTypeRepository OrderStatusTypes => 
            _orderStatusTypes ??= new OrderStatusTypeRepository(_context);

        public void Dispose()
        {
            if(_context != null)
                _context.Dispose();
            _context = null;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public Task SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
