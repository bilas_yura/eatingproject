﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EatingProgram.DAL.Migrations
{
    public partial class seedFunction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "OrderStatusTypes",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "In progress" });

            migrationBuilder.InsertData(
                table: "OrderStatusTypes",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Delivered" });

            migrationBuilder.InsertData(
                table: "OrderStatusTypes",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Canceled" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "OrderStatusTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OrderStatusTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "OrderStatusTypes",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
