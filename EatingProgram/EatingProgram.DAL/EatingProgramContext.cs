﻿using EatingProgram.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace EatingProgram.DAL
{
    public class EatingProgramContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderStatusType> OrderStatusTypes { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }

        public EatingProgramContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.HasMany<Product>(productType => productType.Products)
                    .WithOne(product => product.ProductType)
                    .HasForeignKey(product => product.ProductTypeId);

                entity.Property(productType => productType.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasMany<OrderItem>(product => product.OrderItems)
                    .WithOne(orderItem => orderItem.Product)
                    .HasForeignKey(orderItem => orderItem.ProductId);

                entity.Property(product => product.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<OrderItem>(entity => 
            {
                entity.HasOne(orderItem => orderItem.Order)
                    .WithMany(order => order.OrderItems)
                    .HasForeignKey(orderItem => orderItem.OrderId);

                entity.Property(orderItem => orderItem.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasOne(order => order.User)
                    .WithMany(user => user.Orders)
                    .HasForeignKey(order => order.UserId);

                entity.Property(order => order.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<OrderStatusType>(entity =>
            {
                entity.HasMany<Order>(status => status.Orders)
                    .WithOne(order => order.Status)
                    .HasForeignKey(order => order.StatusId);

                entity.Property(status => status.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<OrderStatusType>().HasData(
                new OrderStatusType() { Id = 1, Name = "InProgress" },
                new OrderStatusType() { Id = 2, Name = "Delivered" },
                new OrderStatusType() { Id = 3, Name = "Canceled" }
            );
        }
    }
}
