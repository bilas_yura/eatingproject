﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("order/{id}/products")]
        public ActionResult<List<ProductDto>> GetByOrderId([FromRoute] int id)
        {
            try
            {
                return _productService.GetByOrderId(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("product-type/{id}/products")]
        public ActionResult<List<ProductDto>> GetByProductType([FromRoute] int id)
        {
            try
            {
                return _productService.GetByProductType(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("products")]
        public ActionResult<List<ProductDto>> Get()
        {
            return _productService.Get();
        }

        [HttpGet("product/{id}")]
        public ActionResult<ProductDto> GetById([FromRoute] int id)
        {
            try
            {
                return _productService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("product/{id}/with-type")]
        public ActionResult<ProductWithTypeDto> GetProductWithTypeById([FromRoute] int id)
        {
            try
            {
                return _productService.GetProductWithTypeById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("product")]
        public ActionResult<ProductDto> Post([FromBody] ProductDto product)
        {
            try
            {
                return _productService.Insert(product);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("product")]
        public ActionResult<ProductDto> Put([FromBody] ProductDto product)
        {
            try
            {
                var prod = _productService.Update(product);
                return Ok(prod);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("product/{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                _productService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
