﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class ProductTypeController : ControllerBase
    {
        private readonly IProductTypeService _productTypeService;

        public ProductTypeController(IProductTypeService productTypeService)
        {
            _productTypeService = productTypeService;
        }

        [HttpGet("product-types")]
        public ActionResult<List<ProductTypeDto>> Get()
        {
            return _productTypeService.Get();
        }

        [HttpGet("product-type/{id}")]
        public ActionResult<ProductTypeDto> GetById([FromRoute] int id)
        {
            try
            {
                return _productTypeService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("product-type")]
        public ActionResult<ProductTypeDto> Post([FromBody] ProductTypeDto productType)
        {
            try
            {
                return _productTypeService.Insert(productType);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("product-type")]
        public ActionResult<ProductTypeDto> Put([FromBody] ProductTypeDto productType)
        {
            try
            {
                var prodType = _productTypeService.Update(productType);
                return Ok(prodType);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("product-type/{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                _productTypeService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
