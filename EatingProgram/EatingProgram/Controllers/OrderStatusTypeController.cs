﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class OrderStatusTypeController : ControllerBase
    {
        private readonly IOrderStatusTypeService _orderStatusTypeService;

        public OrderStatusTypeController(IOrderStatusTypeService orderStatusTypeService)
        {
            _orderStatusTypeService = orderStatusTypeService;
        }

        [HttpGet("order-statuses")]
        public ActionResult<List<OrderStatusTypeDto>> Get()
        {
            return _orderStatusTypeService.Get();
        }

        [HttpGet("order-status/{id}")]
        public ActionResult<OrderStatusTypeDto> GetById([FromRoute] int id)
        {
            try
            {
                return _orderStatusTypeService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
