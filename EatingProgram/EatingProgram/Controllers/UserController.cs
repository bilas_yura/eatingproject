﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("users")]
        public ActionResult<List<UserDto>> Get()
        {
            return _userService.Get();
        }

        [HttpGet("user/{id}")]
        public ActionResult<UserDto> GetById([FromRoute] int id)
        {
            try
            {
                return _userService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("user")]
        public ActionResult<UserDto> Post([FromBody] UserDto user)
        {
            try
            {
                var usr = _userService.Insert(user);
                return Ok(usr);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("user")]
        public ActionResult<UserDto> Put([FromBody] UserDto user)
        {
            try
            {
                var usr = _userService.Update(user);
                return usr;
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("user/{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                _userService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}