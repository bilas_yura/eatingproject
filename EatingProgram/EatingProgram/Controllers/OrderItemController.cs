﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        private readonly IOrderItemService _orderItemService;

        public OrderItemController(IOrderItemService orderItemService)
        {
            _orderItemService = orderItemService;
        }

        [HttpGet("product-type/{id}/order-items")]
        public ActionResult<List<OrderItemDto>> GetByProductType([FromRoute] int id)
        {
            try
            {
                return _orderItemService.GetByProductType(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("order/{id}/order-items")]
        public ActionResult<List<OrderItemDto>> GetByOrderId([FromRoute] int id)
        {
            try
            {
                return _orderItemService.GetByOrderId(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("order-items")]
        public ActionResult<List<OrderItemDto>> Get()
        {
            return _orderItemService.Get();
        }

        [HttpGet("order-item/{id}")]
        public ActionResult<OrderItemDto> GetById([FromRoute] int id)
        {
            try
            {
                return _orderItemService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("order-item")]
        public ActionResult<OrderItemDto> Post([FromBody] OrderItemDto orderItem)
        {
            try
            {
                return _orderItemService.Insert(orderItem);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("order-item")]
        public ActionResult<OrderItemDto> Put([FromBody] OrderItemDto orderItem)
        {
            try
            {
                var ordItem = _orderItemService.Update(orderItem);
                return Ok(ordItem);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("order-item/{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                _orderItemService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
