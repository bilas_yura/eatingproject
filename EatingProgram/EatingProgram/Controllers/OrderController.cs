﻿using System;
using System.Collections.Generic;
using EatingProgram.Core.Abstractions.Services;
using EatingProgram.Core.DTO;
using Microsoft.AspNetCore.Mvc;

namespace EatingProgram.Controllers
{
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService) 
        {
            _orderService = orderService;
        }

        [HttpGet("product/{id}/orders")]
        public ActionResult<List<OrderDto>> GetByProductId([FromRoute] int id)
        {
            try
            {
                return _orderService.GetByProductId(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("product-type/{id}/orders")]
        public ActionResult<List<OrderDto>> GetByProductType([FromRoute] int id)
        {
            try
            {
                return _orderService.GetByProductType(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("user/{id}/orders")]
        public ActionResult<List<OrderDto>> GetByUserId([FromRoute] int id)
        {
            return _orderService.GetByUserId(id);
        }

        [HttpGet("orders/status/{id}")]
        public ActionResult<List<OrderDto>> GetByStatusId([FromRoute] int id)
        {
            try
            {
                return _orderService.GetByStatusId(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("orders")]
        public ActionResult<List<OrderDto>> Get()
        {
            return _orderService.Get();
        }

        [HttpGet("order/{id}")]
        public ActionResult<OrderDto> GetById([FromRoute] int id)
        {
            try
            {
                return _orderService.GetById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("order/{id}/with-items")]
        public ActionResult<OrderWithItemsDto> GetWithItemsById([FromRoute] int id)
        {
            try
            {
                return _orderService.GetOrderWithItemsById(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("order")]
        public ActionResult<OrderDto> Post([FromBody] OrderDto order)
        {
            try
            {
                return _orderService.Insert(order);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("order/with-items")]
        [HttpPost]
        public ActionResult<OrderWithItemsDto> Post([FromBody] OrderWithItemsDto order)
        {
            try
            {
                return _orderService.Insert(order);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("order")]
        public ActionResult<OrderDto> Put([FromBody] OrderDto order)
        {
            try
            {
                var ord = _orderService.Update(order);
                return Ok(ord);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("order/{id}")]
        public IActionResult Delete([FromQuery] int id)
        {
            try
            {
                _orderService.Delete(id);
                return Ok();
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }
    }
}
